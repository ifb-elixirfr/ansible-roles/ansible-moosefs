
Moosefs role
==============
- Install moosefs master and chunkservers
- Configure mounts on moosefs client



Installation
------------

1. Install chunk servers  with pxe "storage" config and master server with "generic" config.

2. Add mfsmaster domain name to your DNS, which should redirect to the master server.

3. Partition and format all disks you want to use with moosefs on chunk servers. Mount them on /mnt/mfschunksX mount point.

For example, if you want to use disk /dev/sdh
```
# create partition
parted --script --align optimal /dev/sdh mklabel gpt mkpart mfschunks1 0% 100%
#Format XFS: if 4k disk
mkfs.xfs -s size=4k /dev/sdh1
#else
mkfs.xfs /dev/sdh1
#
#add entry to fstab
read UUID FS_TYPE < <(blkid ${drive}1 | awk -F"[= ]" '{ print $3" "$5 }' | sed 's/\"//g')
echo -e "UUID=$UUID\t/mnt/mfschunks1\t$FS_TYPE\tdefaults\t0 0" >> /etc/fstab
# create mount point and mount disk
mkdir -p /mnt/mfschuncks1
mount -a
```

4. Configure vars to specify mount points you want to use for your chunk servers. For example,to use half of disks drive on stor-05:
```
moosefs:
  chunkservers:
    - ip: 192.168.16.105
      name: stor-05
      hdds_prod:
          - /mnt/mfschunks1
          - /mnt/mfschunks2
          - /mnt/mfschunks3
          - /mnt/mfschunks4
          - /mnt/mfschunks5
          - /mnt/mfschunks6
      hdds_spare:
          - /mnt/mfschunks6
          - /mnt/mfschunks7
          - /mnt/mfschunks8
          - /mnt/mfschunks9
          - /mnt/mfschunks10
          - /mnt/mfschunks11
          - /mnt/mfschunks12
```

5. Run playbook.



References
------------
https://moosefs.com/support/#documentation 
